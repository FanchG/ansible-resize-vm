Role Name
=========

Resize FileSystem on VM after disk increasing in Promox


Requirements
------------

Should work with default centos and ubuntu installation

Role Variables
--------------



Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

MIT

